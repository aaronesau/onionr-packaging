#!/bin/sh
set -e

rm -f *.build *.buildinfo *.upload *.changes *.dsc *.tar.xz *.tar.gz
cd onionr
git pull
make dpkg
make deb
debuild -S | tee /tmp/debuild.log 2>&1

